#!/usr/bin/env bash
set -euf -o pipefail

dir=$(dirname "$0")
key_dir="${dir}/../private/client"
if [[ -d "$key_dir" ]]; then
    echo "Removing existing $key_dir"
    rm -rf "$key_dir"
fi

mkdir -p "$key_dir"
pushd "$key_dir"

echo Create the client key and CSR
openssl genrsa -out key.pem 4096
openssl req -subj '/CN=client' -new -key key.pem -out client.csr

echo Sign the client cert
echo extendedKeyUsage = clientAuth > extfile.cnf
openssl x509 -req -days 365 -sha256 -in client.csr -CA "${CA_PEM_FNAME:-../ca/ca.pem}" -CAkey "${CA_KEY_FNAME:-../ca/ca-key.pem}" \
  -CAcreateserial -out cert.pem -extfile extfile.cnf

chmod -v 0644 key.pem
chmod -v 0644 cert.pem
rm -v client.csr
popd
